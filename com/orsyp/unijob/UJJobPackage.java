package com.orsyp.unijob;

import com.orsyp.Go;
import com.orsyp.OutcomeReport;
import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.deploymentpackage.ApiConflictPolicy;
import com.orsyp.api.deploymentpackage.ApiPackageType;
import com.orsyp.api.deploymentpackage.Package;
import com.orsyp.api.deploymentpackage.PackageElement;
import com.orsyp.api.deploymentpackage.PackageElementImpl;
import com.orsyp.api.deploymentpackage.PackageId;
import com.orsyp.api.job.Job;
import com.orsyp.api.job.JobId;
import com.orsyp.api.job.JobItem;
import com.orsyp.api.mu.Mu;
import com.orsyp.api.mu.MuId;
import com.orsyp.api.mu.MuItem;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.syntaxerules.UnijobSyntaxRules;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskId;
import com.orsyp.api.task.TaskItem;
import com.orsyp.api.task.TaskPlanifiedData;
import com.orsyp.api.task.TaskSpecificData;
import com.orsyp.api.user.User;
import com.orsyp.api.user.UserId;
import com.orsyp.api.user.UserItem;
import com.orsyp.protocols.JobProtocol.LaunchHourPattern;
import com.orsyp.specfilters.JobSpecFilter;
import com.orsyp.specfilters.SpecFilter;
import com.orsyp.specfilters.TargetSpecFilter;
import com.orsyp.specfilters.UserSpecFilter;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.ConnectionFactory;
import com.orsyp.std.JobListStdImpl;
import com.orsyp.std.JobStdImpl;
import com.orsyp.std.MuListStdImpl;
import com.orsyp.std.MuStdImpl;
import com.orsyp.std.NodeConnectionFactory;
import com.orsyp.std.StdImplFactory;
import com.orsyp.std.TaskListStdImpl;
import com.orsyp.std.TaskStdImpl;
import com.orsyp.std.UserListStdImpl;
import com.orsyp.std.UserStdImpl;
import com.orsyp.std.deploymentpackage.PackageElementStdImpl;
import com.orsyp.std.deploymentpackage.PackageStdImpl;
import com.orsyp.std.nfile.LocalBinaryFile;
import com.orsyp.std.user.UsersStdImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class UJJobPackage extends Thread {

	private String nodeName;
	//private int uJPort;
	private String packageName;
	private Context context;
	private Context UJcontext;	
	private String pckSavePath;
	private Boolean ExportPck;
	public OutcomeReport rep;
	public SpecFilter[] allFilters;
	private Properties pGlobalVariables;
	public boolean verbose;
	private int NumMaxJobPerPackage=300;


	
	
	public UJJobPackage(int MaxNumJob, String nodeName, String packageName, int uJPort, Context context, Context UJcontext, 
			String pckSavePath, Boolean ExportPck, OutcomeReport rep, SpecFilter[] allFilters, Properties pGlobalVariables, boolean verbose) throws UniverseException {
		this.nodeName=nodeName;
		this.context = context;
		this.NumMaxJobPerPackage=MaxNumJob;
		this.UJcontext = UJcontext;
		this.packageName = packageName;
		this.pckSavePath = pckSavePath;
		this.ExportPck = ExportPck;
		this.rep = rep;
		this.allFilters=allFilters;
		this.pGlobalVariables=pGlobalVariables;
		this.verbose=verbose;
	}	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			extractPackage(Go.convertOption(pGlobalVariables.getProperty("UJ_EXTRACT_JOBS"))
					, Go.convertOption(pGlobalVariables.getProperty("UJ_EXTRACT_TARGETS"))
					, Go.convertOption(pGlobalVariables.getProperty("UJ_EXTRACT_USERS"))
					,false);
		} catch (UniverseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void extractPackage(boolean Jobs, boolean Users, boolean Targets, boolean Calendars) throws UniverseException{
			
		int jlcount=0;
		int mlcount=0;
		int ulcount=0;
		//int clcount=0;
		
		/**
		 * Setting Up Package for Object insertion
		 **/
		
			packageName = setPckName(packageName);
	
			/**
			 * Extracting Lists of Objects (Targets, Users & Jobs)
			 **/
			
			com.orsyp.api.job.JobList jl = new com.orsyp.api.job.JobList(UJcontext);
			jl.setImpl(new JobListStdImpl());
			// BSA - Dec 2013 - Required for UV4
		    jl.setSyntaxRules(UnijobSyntaxRules.getInstance());
			
			com.orsyp.api.user.UserList ul = new com.orsyp.api.user.UserList(UJcontext);
			ul.setImpl(new UserListStdImpl());
			// BSA - Dec 2013 - Required for UV4
			ul.setSyntaxRules(UnijobSyntaxRules.getInstance());
			
			com.orsyp.api.mu.MuList ml = new com.orsyp.api.mu.MuList(UJcontext);
			ml.setImpl(new MuListStdImpl());
			// BSA - Dec 2013 - Required for UV4
			ml.setSyntaxRules(UnijobSyntaxRules.getInstance());

		     JobSpecFilter jfilter = (JobSpecFilter) allFilters[1];
		     TargetSpecFilter tfilter = (TargetSpecFilter) allFilters[2];
		     UserSpecFilter ufilter = (UserSpecFilter) allFilters[3];

				try {
					jl.extract();
					ul.extract();
					ml.extract();
					//cl.extract();
					
				} catch (UniverseException u){
				// to do	
				}
				/**
				 * Each type of object follows the same logic:
				 * an object item must be created (from an element of the  retrieved)
				 * an Object ID is created from the Item
				 * the Object itself is copied in an instance of object (context + ID)
				 * the std implementation is put in place
				 * only then => the extraction
				 **/
				int NumOfPackages=0;
			if (Jobs && jl.canExtract()){
				jlcount=jl.getCount();
				
				// Number of packages to extract
				NumOfPackages=jlcount/this.NumMaxJobPerPackage;
				int Rest=jlcount-NumOfPackages*this.NumMaxJobPerPackage;
				if(Rest>0){NumOfPackages++;}

				int pckNum=0;
				while(pckNum<NumOfPackages){
					
					PackageId pckID = new PackageId(packageName+"_"+pckNum,PRODUCT_TYPE.UNIJOB,ApiPackageType.UNIJOB_OBJECTS);
					// BSA - Dec 2013 - Required for UV4
					pckID.setSyntaxRules(UnijobSyntaxRules.getInstance());
					Package SavePck = new Package(pckID);
					SavePck.setContext(context);
					SavePck.setComment("Automatically Generated from Node: "+nodeName+" Pck Number: "+pckNum);
					SavePck.setImpl(new PackageStdImpl());
					SavePck.setContext(context);
					
					for (int j = pckNum*this.NumMaxJobPerPackage; 
							j < (pckNum+1)*this.NumMaxJobPerPackage; j++) {
						
						if(j<jl.getCount()){

						PackageElement pck_el = new PackageElement();
						pck_el.setImpl(new PackageElementStdImpl());
						JobItem it = jl.get(j);	
						JobId ID = it.getIdentifier();
						// BSA - Dec 2013 - Required for UV4
						ID.setSyntaxRules(UnijobSyntaxRules.getInstance());
						Job Ujob = new Job(UJcontext,ID);
						Ujob.setImpl(new JobStdImpl());
						Ujob.extract();
						Ujob.enableSyntaxCheck();
						List<String> filesEmpty = new ArrayList<String>();
						Ujob.setJobFileNames(filesEmpty);	
						if(jfilter.matchespattern(Ujob.getName())){
						rep.addnJobExt(1);
						Ujob.setTargetName("XXX_TARGET_DEFAULT");	
						pck_el.setJob(Ujob);	
						// BSA - Sept 2011 - need to set to OVERWRITE by default otherwise import fails
						ApiConflictPolicy policy = null;
						pck_el.setConflictPolicy(policy.OVERWRITE);
						SavePck.addOrUpdatePackageElement(pck_el);

						}
						}
				}
					// BSA - Jan 7th 2013: Bug in export procedure for HKBN - Save should only be done once per package
					Date mydate = new Date();
					SavePck.setLastModificationDate(new Date());
					SavePck.save(mydate);
					
					if (ExportPck){	
						String ExpPackageName = setExpPckName(packageName,pckNum);
						LocalBinaryFile myFile = new LocalBinaryFile(ExpPackageName);
						SavePck.exportPackage(myFile);
						System.out.println("    %%%   Package: Number "+pckNum+" Extracted" );		
					}
					
					
					pckNum++;
				
			}

			PackageId pckIDObjects = new PackageId(packageName+"_Objects",PRODUCT_TYPE.UNIJOB,ApiPackageType.UNIJOB_OBJECTS);
			// BSA - Dec 2013 - Required for UV4
			pckIDObjects.setSyntaxRules(UnijobSyntaxRules.getInstance());
			Package SavePck = new Package(pckIDObjects);
			SavePck.setComment("Automatically Generated from Node: "+nodeName+" Objects");
			SavePck.setImpl(new PackageStdImpl());
			
			if (Users && ul.canExtract()){
				ulcount=ul.getCount();
				for (int j = 0; j < ul.getCount(); j++) {		
					PackageElement pck_el = new PackageElement();
					pck_el.setImpl(new PackageElementStdImpl());
					UserItem it = ul.get(j);	
					UserId ID = it.getIdentifier();
					ID.setSyntaxRules(UnijobSyntaxRules.getInstance());
					User MyUser = new User(UJcontext,ID);
					MyUser.setImpl(new UserStdImpl());
					MyUser.extract();	
					if(ufilter.matchespattern(MyUser.getName())){
					pck_el.setUser(MyUser);		
					// BSA - Sept 2011 - need to set to OVERWRITE by default otherwise import fails
					ApiConflictPolicy policy = null;
					pck_el.setConflictPolicy(policy.OVERWRITE);
					SavePck.addOrUpdatePackageElement(pck_el);
					} else if(!jfilter.matchespattern(MyUser.getName()) && verbose){
						System.out.println("   %%% user "+MyUser.getName()+" skipped [filtered].");
					}
				}
			}
			if (Targets && ml.canExtract()){	
				mlcount=ml.getCount();
				for (int j = 0; j < ml.getCount(); j++) {		
					PackageElement pck_el = new PackageElement();
					pck_el.setImpl(new PackageElementStdImpl());
					MuItem it = ml.get(j);	
					
					MuId ID = it.getIdentifier();
					ID.setSyntaxRules(UnijobSyntaxRules.getInstance());
					Mu MyMu = new Mu(UJcontext,ID);
					MyMu.setImpl(new MuStdImpl());
					MyMu.extract();	
					if(tfilter.matchespattern(MyMu.getName())){
					pck_el.setMu(MyMu);		
					// BSA - Sept 2011 - need to set to OVERWRITE by default otherwise import fails
					ApiConflictPolicy policy = null;
					pck_el.setConflictPolicy(policy.OVERWRITE);
					SavePck.addOrUpdatePackageElement(pck_el);
					
					}else if(!tfilter.matchespattern(MyMu.getName()) && verbose){
						System.out.println("   %%% Target "+MyMu.getName()+" skipped [filtered].");
					}
				}
			}
			SavePck.setContext(context);
			SavePck.save(new Date());
			if (ExportPck){				
				String ExpPackageName = setExpPckName(packageName,-1);
				LocalBinaryFile myFile = new LocalBinaryFile(ExpPackageName);
				SavePck.exportPackage(myFile);
				System.out.println("    %%%   Package Object Extracted" );
			}
			
			rep.addnPackageExt(1);
			rep.subnExtraction();

			if (ExportPck){				
				
					System.out.println("\n    +++ Extraction Finished for Node: "+nodeName+ " Package(s) Exported at "+pckSavePath);
				}else{
					System.out.println("    +++ Extraction Finished for Node: "+nodeName);
				}
			System.out.println("    %%%   Package: [ Name | nb of Jobs | nb of Users | nb of Targets ]: [ " 
					+packageName+" | "+jlcount+" | "+ulcount+" | "+mlcount+" ]\n" );	
			
			}
	}
	
	public String setPckName(String name){
		if (name.equals("")){
			
	DateFormat dateFormat = new SimpleDateFormat("MMddyyyy_HHmmss");
    Date date = new Date();
    packageName=nodeName+"_"+dateFormat.format(date);
		} else{
			packageName=name;
		}
		return packageName;
	}
	
	public String setExpPckName(String name, int numPck){
		if(numPck==-1){
			return pckSavePath+name+"_Objects"+".unipkg";
		}else{
		return pckSavePath+name+"_"+numPck+".unipkg";
		}
	}



}
