package com.orsyp.specfilters;

import java.util.regex.Pattern;

public class UserSpecFilter  extends SpecFilter{
	
	public String fType="USER";
	
	public UserSpecFilter(String pattern){
		super(pattern);
		isActive=true;
	}
	
	public String getType(){
		return fType;
	}
	
}
