package com.orsyp.help;

public class OnlineHelp {
	public static void displayHelp(int RC, String Message, String Release) {
		
		System.out.println(Message+"\n");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("   This Tool creates one backup package per UJ node (Jobs, Targets & users)");
		System.out.println("                  All Packages are stored on the UVMS    ");		
		System.out.println("---------------------------------------------------------------------------\n");
		System.out.println("++ Optional Parameters:\n");
		System.out.println("    --login:     UVMS connection login     [can be specified in properties file]");
		System.out.println("    --pwd:       UVMS connection password  [can be specified in properties file]");
		System.out.println("    --uvms:      UVMS hostname             [can be specified in properties file]");
		System.out.println("    --port:      UVMS Connection port      [can be specified in properties file]");
		System.out.println("    --pckname:   Force Package Name to a specific value [by default, packages are named NODE_TIMESTAMP]");
		System.out.println("    --extconcur: Restrict the number of concurrent package extraction");
		System.out.println("    --purge:     Force Retention Delay for Purge [Format is DD:HH:MM]");
		System.out.println("    --sim:       SIMULATION Mode, no extraction occurs. Returns a list of target nodes.");
		System.out.println("    --maxsimext: Number of Maximum Simultaneous Extractions for Multithreading Mode.");
		System.out.println("");
		System.out.println("++ Filtering Parameters:\n");
		System.out.println("    --job:      Restrict the extraction to specific Job Names [supports Regular Expressions]");
		System.out.println("    --user:     Restrict the extraction to specific User Names [supports Regular Expressions]");
		System.out.println("    --target:   Restrict the extraction to specific Target Names [supports Regular Expressions]");
		System.out.println("    --node:     Restrict the Pck Extraction to specific Nodes [supports Regular Expressions]");
		System.out.println("    --pck:      Restrict the PURGE to specific Package Names [supports Regular Expressions]");
		System.out.println("");
		System.out.println("++ Display, Help & Version Info:\n");
		System.out.println("    --help:    Display online help");
		System.out.println("    --version: Version Information & UVMS compatibility");
		System.out.println("    --verbose: Display Extra Information");
		System.out.println("");
		System.out.println("  IMPORTANT NOTE:");
		System.out.println("  => All Configuration parameters can be modified from properties file in same folder as Jar file");
		System.out.println("");
		System.out.println("++ Examples:\n");
		System.out.println("    pckExtract --pwd=universe");
		System.out.println("    pckExtract --pwd=universe --pckname=MyPackage");
		System.out.println("    pckExtract --pwd=universe --pckname=MyPackage --job=/OPT.*");
		System.out.println("    pckExtract --pwd=universe --pckname=MyPackage --pck=BSALINUX.* --purge=01:00:00");
		System.out.println("");
		System.out.println("++ Important Note on Regular Expressions:\n");
		System.out.println("    Regular Expression compatibility has been implemented for all filtering options, here are some pattern examples");
		System.out.println("    .*         => all objects (note that in RegExp '.' denotes ANY character");
		System.out.println("    BSA.*      => all objects starting with BSA");
		System.out.println("    ^[0-9]*.*  => all objects starting with a sequence of n numbers followed by any set of n characters");

		System.exit(RC);
	}

	public static void displayVersion(int RC, String release, String[] APIversions) {
		System.out.println("Version "+release);
		System.out.println("++ This Tool was compiled and tested with the following UVMS:");
		for (int j=0;j < APIversions.length;j++){
			System.out.println(APIversions[j]);
		}
		System.out.println("  => Other versions of UVMS might not work as expected or might not work at all");
		System.exit(RC);
	}
}
