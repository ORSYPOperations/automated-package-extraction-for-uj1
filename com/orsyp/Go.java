package com.orsyp;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.orsyp.specfilters.JobSpecFilter;
import com.orsyp.specfilters.NodeSpecFilter;
import com.orsyp.specfilters.PackageSpecFilter;
import com.orsyp.specfilters.SpecFilter;
import com.orsyp.specfilters.TargetSpecFilter;
import com.orsyp.specfilters.UserSpecFilter;
import com.orsyp.std.central.network.NetworkNodeListStdImpl;
import com.orsyp.unijob.UJJobPackage;
import com.orsyp.unijob.UJPackageClear;
import com.orsyp.unijob.UVMSInterface;
import com.orsyp.util.PropertyLoader;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.Product;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.central.UniCentral.LicenseEntryStatus;
import com.orsyp.api.central.networknode.NetworkNodeId;
import com.orsyp.api.central.networknode.NetworkNodeItem;
import com.orsyp.api.central.networknode.NetworkNodeList;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.node.NodeId;
import com.orsyp.help.*;
import edu.mscd.cs.jclo.JCLO;


/**
 * INTERNAL RELEASE NOTES:
 * 16 Sept 2011 :BSA: added multi threaded extractions & Now forces the conflict policy to "OVERWRITE"
 * Dec 5 2012: BSA: adaptation for HKBN
 * Dec 2013: BSA: adapted for UV6 (ex UV4)
 *
 **/


class AllArgs {
    private boolean help;
    private String pwd;
    private String login;
    private int port;
    private String uvms;
    private String pckname;
    private boolean version;
    private String purge;
    private String job;
    private String node;
    private String user;
    private String target;
    private boolean verbose;
    private boolean sim;
    private String pck;
    private int maxsimext;
}

public class Go {
	// a comment
	
	static String Customer = "HKBN";
	static String Release="2.0.0";
	static String[] APIversions = {"600"};
	
	public static void main(String[] argv) throws IOException, InterruptedException {
			
		Boolean ExportPck=false;
		String type="UNIJOB";
		String DefaultRetDelay="10:00:00";
		boolean purgeActive=true;
		int DayS=0;
		int HourS=0;
		int MinuteS=0;
		
		// UJ port is retrieved from UVMS once connected
		int UJPort;
		String password="";
		String login="";
		String UVMSHost="";
		int port=0;
		String packageName="";
		String pckSavePath="";
		boolean displayHelp=false;
		boolean displayVer=false;
		String RetentionDelay;
		String nodeFilter="";
		String userFilter="";
		String targetFilter="";
		String jobFilter="";
		String packageFilter="";
		int extconcur = 0;
		boolean verbose=false;
		boolean sim=false;
		boolean LicExpires=false;
		int MaxNumJobPerPackage=0;
		
		try {
				System.out.println("=======================================================");
				System.out.println("  **  ORSYP Package Extraction Tool version " + Release +"  **  ");
				System.out.println("  *          ORSYP Professional Services.           * ");
				System.out.println("  * Copyright (c) 2012 ORSYP.  All rights reserved. * ");
				System.out.println("  **             Implemented for "+Customer+"              **");
				System.out.println("=======================================================");
				System.out.println("");

				OutcomeReport rep = new OutcomeReport();
				
				//Retrieveing all parameters; see help file for more detail
		        JCLO jclo = new JCLO (new AllArgs());
		        jclo.parse (argv);
		        
		        displayHelp=jclo.getBoolean ("help");
		        displayVer=jclo.getBoolean ("version");
		        password=jclo.getString ("pwd");
		        login=jclo.getString ("login");
		        UVMSHost=jclo.getString ("uvms");
		        port=jclo.getInt ("port");
		        packageName=jclo.getString ("pckname");
		        RetentionDelay=jclo.getString ("purge");
		        extconcur=jclo.getInt("maxsimext");
		        jobFilter=jclo.getString ("job");
		        targetFilter=jclo.getString ("target");
		        nodeFilter=jclo.getString ("node");
		        userFilter=jclo.getString ("user");
		        packageFilter=jclo.getString ("pck");
		        verbose=jclo.getBoolean ("verbose");
		        sim=jclo.getBoolean ("sim");
		        
		        if(displayHelp){OnlineHelp.displayHelp(0,"Display help",Release);}
		        if(displayVer){OnlineHelp.displayVersion(0,Release,APIversions);}
			    
		        // Some parameters can be empty but are mandatory.. such as password, login etc for UVMS
		        // in that case, we take into account the parameter passed, and if none, we take the default one from properties file
		        Properties pGlobalVariables = PropertyLoader.loadProperties("ExtUJPackages");
			    if (password==null || password.equals("")){password=pGlobalVariables.getProperty("UVMS_PWD");}    
			    if (password==null || password.equals("")){OnlineHelp.displayHelp(-1,"Error: No Password Passed!",Release);}
			         
				System.out.println("=> Loading Program Options...");
				
				ExportPck=convertOption(pGlobalVariables.getProperty("UJ_EXTRACT_JOBS"));
				purgeActive=convertOption(pGlobalVariables.getProperty("PURGE_ACTIVE"));
				
				if(pGlobalVariables.getProperty("MAX_JOBS_PER_PACKAGE")!=null){
					MaxNumJobPerPackage=Integer.valueOf(pGlobalVariables.getProperty("MAX_JOBS_PER_PACKAGE"));
				}else{
				 MaxNumJobPerPackage=99999999; // consider this value as infinite..
				}
				if (MaxNumJobPerPackage==0){MaxNumJobPerPackage=99999999;}
				
	            if (jobFilter == null){jobFilter="";}
	            if (targetFilter == null){targetFilter="";}
	            if (nodeFilter == null){nodeFilter="";}
	            if (userFilter == null){userFilter="";}
	            if (packageFilter == null){packageFilter="";}
	            
		        SpecFilter[] allFilters = new SpecFilter[5];
		        NodeSpecFilter nfilter = new NodeSpecFilter(nodeFilter);
		        JobSpecFilter jfilter = new JobSpecFilter(jobFilter);
		        TargetSpecFilter tfilter = new TargetSpecFilter(targetFilter);
		        UserSpecFilter ufilter = new UserSpecFilter(userFilter);
		        PackageSpecFilter pfilter = new PackageSpecFilter(packageFilter);
		        
		        allFilters[0]=nfilter;
		        allFilters[1]=jfilter;
		        allFilters[2]=tfilter;
		        allFilters[3]=ufilter;
		        allFilters[4]=pfilter;
	            
				if (pckSavePath == null || pckSavePath.equals("")){pckSavePath=pGlobalVariables.getProperty("UVMS_SAVE_PATH");}
	            if (login == null || login.equals("")){login=pGlobalVariables.getProperty("UVMS_USER");}
	            if (UVMSHost == null || UVMSHost.equals("")){UVMSHost=pGlobalVariables.getProperty("UVMS_HOST");}
	            if (port==0){port=Integer.parseInt(pGlobalVariables.getProperty("UVMS_PORT"));}
	            if (extconcur==0){extconcur=Integer.parseInt(pGlobalVariables.getProperty("UJ_MAX_SIM_EXTRACT"));}
	            if (packageName == null){packageName="";}
	            if(RetentionDelay==null||RetentionDelay==""){RetentionDelay=pGlobalVariables.getProperty("RETENTION_DELAY");}            
	            
	            if (RetentionDelay.equals("") && purgeActive){
	            	System.out.println("  !!! RETENTION_DELAY variable is missing... Default value: "+DefaultRetDelay);
	            	RetentionDelay=DefaultRetDelay;}
	            
	            if (purgeActive){
	            String[] DateShift=RetentionDelay.split(":");
	            try{
	            	DayS=Integer.parseInt(DateShift[0]);
	            	HourS=Integer.parseInt(DateShift[1]);
	            	MinuteS=Integer.parseInt(DateShift[2]);
	            }catch(ArrayIndexOutOfBoundsException a1){
	            	System.out.println("  --- RETENTION_DELAY variable Format is wrong... Purge is deactivated.");
	            	DayS=0;
	            	HourS=0;
	            	MinuteS=0;
	            	purgeActive=false;
	            }
	            }

	            if(UVMSHost==null||password==null||login==null||UVMSHost==""||port==0||password==""||login==""){
	            	System.out.println("-- Fatal Error in Parameters Passed [ missing login, password, hostname or port ].");
	            	OnlineHelp.displayHelp(1,"Error in Parameters",Release);
	            }
	            
				UVMSInterface uvInterface = new UVMSInterface(UVMSHost,port,login,password,pGlobalVariables.getProperty("DU_NODE"),pGlobalVariables.getProperty("DU_COMPANY"),Area.Exploitation);
				
				UniCentral central = uvInterface.ConnectEnvironment();
				UJPort = Integer.parseInt(central.getCentralConfigurationVariable("DEF_UJ_IO_PORT"));
				boolean supVersion=false;
				for (int k=0;k<APIversions.length;k++){
					if (String.valueOf(central.getVersion()).equals(APIversions[k])){
						supVersion=true;
					}
				}
				if (! supVersion){
					//System.out.println("   --- Warning ! Current Version of UVMS ["+central.getVersion()+"] does not seem to be supported.");
					//System.out.println("   --- Program might not work as expected: Please Check Version Compatibility with option -v...\n");
					}
				
				/**
				 * Retrieve the node list from UVMS, all parameters can be seen below, however we only use some
				 * and we filter on Unijob Nodes only
				 */
				
				/**
				 *  As a standard, we check for license validity as we no longer deliver full modules without being paid
				 */
				boolean checkLicActive=false;
				if(checkLicActive){
	            
				
					String LicStr=pGlobalVariables.getProperty("MODULE_LIC");
					boolean RepLicOK=false;
					String LicStat="";
					boolean RepLicFound=true;
				
					// Below is legacy code for CLSA - Removing for HKBN
					
					/*
					if (LicStr==null || LicStr.equals("")){
					 try{
							String RootDir = central.getCentralConfigurationVariable("UNI_DIR_ROOT").replace("\\", "/");
							String LicFile=RootDir+"/data/licenses.txt";
	
						  FileInputStream fstream = new FileInputStream(LicFile);
						  DataInputStream in = new DataInputStream(fstream);
						  BufferedReader br = new BufferedReader(new InputStreamReader(in));
						  String strLine;
						  
						  while ((strLine = br.readLine()) != null)   {
							  if(strLine.contains("REPORTER")){
								  RepLicFound=true;
							  LicenseEntryStatus myLicStat= central.checkLicenseEntry(strLine);
							  if(myLicStat.toString().equals("VALID")){
								  RepLicOK=true;
								  LicStat=myLicStat.toString();
							  }else{LicStat=myLicStat.toString();}
							  }
							  
						  }
						  in.close();
						  
						    }catch (Exception e){
						  System.err.println("Error: " + e.getMessage());
						  }
					}else{
						*/

					if (LicStr==null || LicStr.equals("")){RepLicFound=false;System.out.println("  --- ERROR - No Licence for " + Customer + " Module Found.");System.exit(99);}
					else{
						
						if( ! LicStr.equals("4569dhgierhxml6789")){
						LicenseEntryStatus myLicStat= central.checkLicenseEntry(LicStr);  
						if(myLicStat.toString().equals("VALID")){
							RepLicOK=true;
							// extracting the expiration date in licence line
							String DateExp=LicStr.split(" ")[5].substring(6);
							if (DateExp.equals("20361231")){LicExpires=false;}
								LicStat=myLicStat.toString();
						    }else{
						    	LicStat=myLicStat.toString();
						    }	    
						} else {
							RepLicOK=true;
							LicExpires=false;
							LicStat="VALID";
						}
					}
					
					if(!RepLicOK){
						System.out.println("  --- ERROR - Licence for " + Customer + " Module on UVMS has Status: "+LicStat);
						System.exit(99);
					}
				}
				
				// 1- we first extract the list of all nodes declared on UVMS
				NetworkNodeList networkList = new NetworkNodeList(uvInterface.getContext());
				networkList.setImpl(new NetworkNodeListStdImpl());
				try {
					networkList.extract();
				} catch (UniverseException e1) {
					e1.printStackTrace();
					return;
				}
				// - Simulation Mode in order to check what will be extracted in real mode
				if(sim){
					System.out.println("=> + SIMULATION MODE: No extraction will take place +\n");
					//System.out.println("=>    Following list of nodes would be extracted:\n");
					
				
					for (int i = 0; i < networkList.getCount(); i++) {
						NetworkNodeItem item = networkList.get(i);
						String NodeName=item.getNodeCompanyConfiguration().getNodeName();
						PRODUCT_TYPE ProdType=item.getNodeCompanyConfiguration().getProductType();
						int NodeStat = item.getNodeInfo().getStatus();
					
						if (item.getNodeCompanyConfiguration().getProductType().toString().equals(type) && nfilter.matchespattern(NodeName)){
							System.out.println(NodeName
									+ ", Type [" + ProdType + "]"
									+ ", OS [" + item.getNodeInfo().getOs() + "]"
									+ ", Status [" + NodeStat +"]");
						}
				}
					System.out.println("\n=> + END OF SIMULATION +");
					System.exit(0);
				}
				
				// Ryan LO - Sept 2011 - implementing parallel threading extractions for better performances
				List <Thread> pakExtractions = new ArrayList <Thread>();
				List <Context> AllConContexts = new ArrayList <Context>();
				// BSA - Sept 2011 - reviewed the output of program
				System.out.println("=> MAX SIMULTANEOUS EXTRACTION(S) ["+extconcur+"]");
				System.out.println("=> NUMBER OF JOBS MAX PER PACKAGE ["+MaxNumJobPerPackage+"]");
				System.out.println("=> Available Nodes Selected for Extraction:");
				// BSA - Jan 7th 2013: Bug in export procedure for HKBN
				extconcur=1;
				for (int i = 0; i < networkList.getCount(); i++) {
					NetworkNodeItem item = networkList.get(i);
					
					
					String NodeName=item.getNodeCompanyConfiguration().getNodeName();
					PRODUCT_TYPE ProdType=item.getNodeCompanyConfiguration().getProductType();
					int NodeStat = item.getNodeInfo().getStatus();
				
					if (item.getNodeCompanyConfiguration().getProductType().toString().equals(type) && nfilter.matchespattern(NodeName)){

					if (item.getNodeInfo().getStatus() != 1){
						System.out.println("    --- Could not connect to Node "+item.getNodeCompanyConfiguration().getNodeName());
						System.out.println("    --> Skipping Node.");
						rep.addnameUJNodesKO(item.getNodeCompanyConfiguration().getNodeName());
						rep.addnUJNodesKO(1);
					} else {

					String group = "*";
					String host = "*";
					String system = "W32";
					Environment environment = null;
					// 2- we store all contexts with valid connection for parallele processing (all valid UJ nodes)
					try {
						environment = new Environment(ProdType.toString(), NodeName);
					} catch (SyntaxException e) {
						e.printStackTrace();
					}
					Client clientUJ = new Client(new Identity(login, group, host, system));
					Context contextUJ = new Context (environment, clientUJ, central);
					contextUJ.setProduct(Product.UNIJOB);
					rep.addnUJNodesOK(1);
					rep.addnExtraction();
					System.out.println("  --> Node ["+NodeName+"] Product ["+ProdType+"] Status ["+NodeStat+"]");
					AllConContexts.add(contextUJ);
					}
					}
									
					else if (item.getNodeCompanyConfiguration().getProductType().toString().equals(type) 
							&& ! nfilter.matchespattern(NodeName) && verbose){
						System.out.println("=> Node "+NodeName+" Skipped [filtered].");
					}
				}
				// BSA - Sept 2011 - The main idea is to build an List of Context that can be connected to
				// Then, take to first extconcur elements of it, and start the extractions, then proceed again.. until completion of the list

				while (AllConContexts.size() > 0){
					
					List <Context> extArray = new ArrayList <Context>();

					for (int el=0;el<extconcur;el++){
						if (!AllConContexts.isEmpty()){
						extArray.add(el, AllConContexts.get(0));
						AllConContexts.remove(0);
						}
					}
					System.out.println("\n=> Extraction(s) Started For: "+extArray.size()+" Node(s).");
					
					for (int ctx=0;ctx<extArray.size();ctx++){
						Context myContext = extArray.get(ctx);
						Environment myEnv = myContext.getEnvironment();
						String nName = myEnv.getNodeName();
						pakExtractions.add(new UJJobPackage(MaxNumJobPerPackage, nName, packageName, UJPort, uvInterface.getContext(), myContext, 
								pckSavePath, ExportPck, rep, allFilters, pGlobalVariables, verbose));
								pakExtractions.get(pakExtractions.size() - 1).start();
					}
					for (int i = 0; i < pakExtractions.size(); i++) {
						if (pakExtractions.get(i).isAlive()) {
							pakExtractions.get(i).join();
						}
					}
				}

				rep.DisplayReport();
				if (purgeActive){
					System.out.println("\n=> Cleaning Packages.");
				UJPackageClear pckClear = new UJPackageClear(purgeActive,DayS,HourS,MinuteS,uvInterface.getContext(), pckSavePath
						, allFilters, verbose);
				pckClear.cleanupPackage();
				}
				
				System.out.println("\n=> End of UJ Package Extractions.");
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		
		if(LicExpires){System.out.println(" !!! You have no permanent license, program will abort !!!");System.exit(99);}
		if(!LicExpires){System.exit(0);}
	}
			
public static boolean convertOption(String s){
	if (s.equalsIgnoreCase("O") || s.equalsIgnoreCase("Y")){
		return true;
	}
	return false;
}



}